package com.sathish.voiceappgupshup.prefs

import android.content.ContentProvider
import android.content.ContentValues
import android.content.Context
import android.net.Uri

class ContextKeeper: ContentProvider() {

    companion object {
        lateinit var appContext: Context
    }

    override fun onCreate(): Boolean {
        appContext = context!!
        return false
    }

    override fun query(
        p0: Uri,
        p1: Array<out String>?,
        p2: String?,
        p3: Array<out String>?,
        p4: String?
    ) = null

    override fun getType(p0: Uri): String? = null

    override fun insert(p0: Uri, p1: ContentValues?): Uri? = null

    override fun delete(p0: Uri, p1: String?, p2: Array<out String>?) = 0

    override fun update(p0: Uri, p1: ContentValues?, p2: String?, p3: Array<out String>?) = 0
}
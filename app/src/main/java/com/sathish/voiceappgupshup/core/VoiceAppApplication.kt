package com.sathish.voiceappgupshup.core

import android.app.Application
import com.sathish.voiceappgupshup.prefs.ContextKeeper
import com.sathish.voiceappgupshup.ui.activity.main.di.selectTypeViewModule
import com.sathish.voiceappgupshup.ui.speechToText.speechToTextActivityModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

import org.koin.core.logger.Level

class VoiceAppApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        ContextKeeper.appContext = this

        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@VoiceAppApplication)
            modules(
                speechToTextActivityModule,
                selectTypeViewModule
            )
        }
    }
}
package com.sathish.voiceappgupshup.extensions

import android.view.View
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment

fun Fragment.applyTopInset(vararg views: View) {
    ViewCompat.setOnApplyWindowInsetsListener(views.first()) { _, insets ->
        views.forEach {
            it.updatePadding(top = insets.systemWindowInsetTop)
        }
        insets
    }
}
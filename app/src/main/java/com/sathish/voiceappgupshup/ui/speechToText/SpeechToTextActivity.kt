package com.sathish.voiceappgupshup.ui.speechToText

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.github.florent37.runtimepermission.kotlin.PermissionException
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.sathish.voiceappgupshup.R
import com.sathish.voiceappgupshup.architecture.BaseActivity
import com.sathish.voiceappgupshup.databinding.ActivitySpeechToTextBinding
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import java.util.*

class SpeechToTextActivity : BaseActivity<SpeechToTextViewModel>() {

    override val layoutId: Int = R.layout.activity_speech_to_text
    override val viewModel: SpeechToTextViewModel by inject()
    private val viewBinding: ActivitySpeechToTextBinding by viewBinding(R.id.container)

    private var speechRecognizer: SpeechRecognizer? = null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityCreated(savedInstanceState: Bundle?) {

        if (!isAudioPermissionsGranted) {
            askAudioPermissionAndRunAudioUpdates()
        } else {
            setupSpeechRecognizer()
        }
    }

    private fun setupSpeechRecognizer() {
        val speechRecognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this)

        speechRecognizerIntent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())

        speechRecognizer?.setRecognitionListener(object : RecognitionListener {
            override fun onReadyForSpeech(bundle: Bundle) {}
            override fun onBeginningOfSpeech() {
                viewBinding.text.setText("")
                viewBinding.text.hint = "Listening..."
            }

            override fun onRmsChanged(v: Float) {}
            override fun onBufferReceived(bytes: ByteArray) {}
            override fun onEndOfSpeech() {}
            override fun onError(i: Int) {}
            override fun onResults(bundle: Bundle) {
                viewBinding.buttonRecord.setImageResource(R.drawable.ic_mic_black_off)
                val data = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                viewBinding.text.setText(data?.get(0))
            }

            override fun onPartialResults(bundle: Bundle) {}
            override fun onEvent(i: Int, bundle: Bundle) {}
        })

        viewBinding.buttonRecord.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, motionEvent: MotionEvent?): Boolean {
                if (motionEvent?.action == MotionEvent.ACTION_UP) {
                    speechRecognizer?.stopListening()
                }
                if (motionEvent?.action == MotionEvent.ACTION_DOWN) {
                    viewBinding.buttonRecord.setImageResource(R.drawable.ic_mic_red_24dp)
                    speechRecognizer?.startListening(speechRecognizerIntent)
                }
                return false
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun askAudioPermissionAndRunAudioUpdates() {
        baseContext?.let {
            if (ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                showAlertDialog(
                    titleResource = R.string.audio_permission_disclosure_title,
                    descriptionResource = R.string.audio_permission_disclosure_message,
                    onPositiveClicked = {
                        checkPermission()
                    }
                )
            } else {
                setupSpeechRecognizer()
            }
        }
    }

    private fun checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            lifecycleScope.launch {
                try {
                    askPermission(
                        Manifest.permission.RECORD_AUDIO
                    ) {
                        setupSpeechRecognizer()
                    }.onDeclined {
                        if (it.hasForeverDenied()) {
                            showToastMessage(getString(R.string.error_disabled_forever_audio))
                            it.goToSettings()
                            finish()
                        } else {
                            AlertDialog.Builder(this@SpeechToTextActivity)
                                .setMessage(getString(R.string.error_need_audio_permission))
                                .setCancelable(false)
                                .setPositiveButton(getString(R.string.button_allow_audio)) { _, _ ->
                                    askAudioPermissionAndRunAudioUpdates()
                                }.setNegativeButton(getString(R.string.button_leave_app)) { _, _ ->
                                    finish()
                                }.show()
                        }
                    }
                } catch (e: PermissionException) {
                    e.printStackTrace()
                    e.goToSettings()
                }
            }
        }
    }

    override fun subscribeOnLiveData() {

    }

    override fun onDestroy() {
        super.onDestroy()
        speechRecognizer?.destroy()
    }
}

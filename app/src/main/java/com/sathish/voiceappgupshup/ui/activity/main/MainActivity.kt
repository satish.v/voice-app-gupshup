package com.sathish.voiceappgupshup.ui.activity.main

import android.os.Bundle
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.sathish.voiceappgupshup.R
import com.sathish.voiceappgupshup.architecture.BaseActivity
import com.sathish.voiceappgupshup.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import by.kirich1409.viewbindingdelegate.viewBinding

class MainActivity: BaseActivity<MainViewModel>() {

    override val layoutId: Int = R.layout.activity_main
    override val viewModel: MainViewModel by viewModel()
    private val viewBinding: ActivityMainBinding by viewBinding(R.id.container)

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<SelectTypeFragment>(R.id.host)
            }
        }
    }
}
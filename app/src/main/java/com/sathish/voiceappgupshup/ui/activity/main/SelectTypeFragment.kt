package com.sathish.voiceappgupshup.ui.activity.main

import android.content.Intent
import android.os.Bundle
import com.sathish.voiceappgupshup.R
import com.sathish.voiceappgupshup.architecture.BaseFragment
import com.sathish.voiceappgupshup.databinding.FragmentSelectTypeBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sathish.voiceappgupshup.extensions.applyTopInset
import com.sathish.voiceappgupshup.ui.googleSpeechToText.GoogleSpeechToTextActivity
import com.sathish.voiceappgupshup.ui.speechToText.SpeechToTextActivity

class SelectTypeFragment: BaseFragment<SelectTypeViewModel>() {
    override val layoutId: Int = R.layout.fragment_select_type
    override val viewModel: SelectTypeViewModel by viewModel()

    private val viewBinding: FragmentSelectTypeBinding by viewBinding()

    override fun onFragmentCreated(savedInstanceState: Bundle?) {

        with(viewBinding){
            applyTopInset(viewBinding.root)
            speechToTextRecognizer.setOnClickListener {

                val intent = Intent(context, SpeechToTextActivity::class.java)
                startActivity(intent)
            }

            speechToTextGoogle.setOnClickListener {

                val intent = Intent(context, GoogleSpeechToTextActivity::class.java)
                startActivity(intent)
            }

        }
    }
}
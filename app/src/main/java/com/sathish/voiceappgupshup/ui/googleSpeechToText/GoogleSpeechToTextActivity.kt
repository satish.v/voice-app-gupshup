package com.sathish.voiceappgupshup.ui.googleSpeechToText

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sathish.voiceappgupshup.R
import com.sathish.voiceappgupshup.architecture.BaseActivity
import com.sathish.voiceappgupshup.databinding.ActivityGoogleSpeechToTextBinding
import org.koin.android.ext.android.inject
import java.util.*

class GoogleSpeechToTextActivity : BaseActivity<GoogleSpeechToTextViewModel>() {

    override val layoutId: Int = R.layout.activity_google_speech_to_text
    override val viewModel: GoogleSpeechToTextViewModel by inject()

    private val viewBinding: ActivityGoogleSpeechToTextBinding by viewBinding(R.id.container)
    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // There are no request codes
                val data: Intent? = result.data
                val res: ArrayList<String>? = data?.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS
                )
                viewBinding.tvSpeechToText.text = res?.get(0)
            }
        }

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        with(viewBinding) {
            ivMic.setOnClickListener {

                val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
                intent.putExtra(
                    RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
                )
                intent.putExtra(
                    RecognizerIntent.EXTRA_LANGUAGE,
                    Locale.getDefault()
                )
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                    getString(R.string.speech_prompt))

                try {
                    resultLauncher.launch(intent)

                } catch (e: Exception) {
                    Toast
                        .makeText(
                            this@GoogleSpeechToTextActivity, e.message,
                            Toast.LENGTH_SHORT
                        )
                        .show()
                }
            }
        }
    }
}
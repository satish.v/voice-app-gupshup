package com.sathish.voiceappgupshup.ui.activity.main.di

import com.sathish.voiceappgupshup.ui.activity.main.SelectTypeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val selectTypeViewModule = module {
    viewModel { SelectTypeViewModel() }
}
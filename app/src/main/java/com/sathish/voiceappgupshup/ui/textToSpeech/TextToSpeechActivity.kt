package com.sathish.voiceappgupshup.ui.textToSpeech

import android.os.Bundle
import by.kirich1409.viewbindingdelegate.viewBinding
import com.sathish.voiceappgupshup.R
import com.sathish.voiceappgupshup.architecture.BaseActivity
import com.sathish.voiceappgupshup.databinding.ActivityTextToSpeechBinding
import org.koin.android.ext.android.inject

class TextToSpeechActivity : BaseActivity<TextToSpeechViewModel>() {
    override val layoutId: Int = R.layout.activity_text_to_speech
    override val viewModel: TextToSpeechViewModel by inject()

    private val viewBinding: ActivityTextToSpeechBinding by viewBinding(R.id.container)

    override fun onActivityCreated(savedInstanceState: Bundle?) {

    }

}
package com.sathish.voiceappgupshup.ui.speechToText

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val speechToTextActivityModule = module {
    viewModel { SpeechToTextViewModel() }
}
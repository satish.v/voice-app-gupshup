package com.sathish.voiceappgupshup.architecture

import android.Manifest
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.sathish.voiceappgupshup.dsl.Alert

abstract class BaseActivity<baseViewModel: BaseViewModel> : AppCompatActivity() {

    abstract val layoutId: Int
    abstract val viewModel: baseViewModel
    private var currentAlertOnScreen: Alert? = null

    open fun onActivityCreated(savedInstanceState: Bundle?){}

    open fun subscribeOnLiveData(){}

    open fun logOut(){}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)

        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
            statusBarColor = Color.TRANSPARENT
        }

        onActivityCreated(savedInstanceState)
        subscribeOnLiveData()
    }

    protected val isAudioPermissionsGranted: Boolean
        get() = baseContext?.let {
            ContextCompat.checkSelfPermission(
                it,
                Manifest.permission.RECORD_AUDIO
            )
        } == PackageManager.PERMISSION_GRANTED

    fun showAlertDialog(
        @StringRes titleResource: Int,
        @StringRes descriptionResource: Int? = null,
        onPositiveClicked: ((DialogInterface) -> Unit)? = null,
        onNegativeClicked: ((DialogInterface) -> Unit)? = null
    ) {
        if (currentAlertOnScreen != null) return
        currentAlertOnScreen = Alert.alert {
            alertContext = this@BaseActivity
            title = titleResource
            description = descriptionResource
            positiveButton = onPositiveClicked
            negativeButton = onNegativeClicked
            onDismiss = {
                currentAlertOnScreen = null
            }
        }
    }

    protected fun showToastMessage(text: String) {
        applicationContext?.let {
            Toast.makeText(it, text, Toast.LENGTH_LONG)
                .show()
        }
    }
}
package com.sathish.voiceappgupshup.architecture

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.cancel

abstract class BaseViewModel : ViewModel(), LifecycleObserver {

    protected val _messages = SingleLiveEvent<Int>()
    val messages: LiveData<Int>
        get() = _messages

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}
package com.sathish.voiceappgupshup.architecture

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.sathish.voiceappgupshup.dsl.Alert
import com.sathish.voiceappgupshup.extensions.toPx

abstract class BaseFragment<T : BaseViewModel> : Fragment() {
    abstract val layoutId: Int
    abstract val viewModel: T

    private var currentAlertOnScreen: Alert? = null

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)
    }

    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(viewModel)
        onFragmentCreated(savedInstanceState)
        subscribeOnLiveData()
    }

    open fun onFragmentCreated(savedInstanceState: Bundle?) {}

    open fun subscribeOnLiveData() {

        viewModel.messages.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT)
                .show()
        }
    }

    fun showAlertDialog(
        @StringRes titleResource: Int,
        @StringRes descriptionResource: Int? = null,
        onPositiveClicked: ((DialogInterface) -> Unit)? = null,
        onNegativeClicked: ((DialogInterface) -> Unit)? = null
    ) {
        if (currentAlertOnScreen != null) return
        currentAlertOnScreen = Alert.alert {
            alertContext = requireContext()
            title = titleResource
            description = descriptionResource
            positiveButton = onPositiveClicked
            negativeButton = onNegativeClicked
            onDismiss = {
                currentAlertOnScreen = null
            }
        }
    }

    protected fun showToastMessage(text: String) {
        context?.let {
            Toast.makeText(it, text, Toast.LENGTH_LONG)
                .show()
        }
    }

    protected fun showSnackbar(text: String, actionLabel: String = "", action: () -> Unit = {}) {
        view?.let {
            val snackbar = Snackbar.make(
                it,
                text,
                Snackbar.LENGTH_LONG
            )
            if (actionLabel.isNotEmpty()) {
                snackbar.setAction(actionLabel) { action() }
            }
            snackbar.view.translationY = -44.toPx().toFloat()
            snackbar.view.elevation = 24.toPx().toFloat()
            snackbar.show()
        }
    }
}
